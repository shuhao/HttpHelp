package http;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;

/**
 * 外层访问接口
 * 需要做的是，用户输入访问的url，参数Map，访问类型（GET，POST......）
 * get，post传参的方式不同
 * 需要考虑的是访问的协议分为http以及https
 * https的时候分为单向验证和双向验证
 * 失败重试机制
 * 链接池的应用
 * 
 * 可以抽出来的部分，
 * 
 * @author niki
 *
 */
public interface HttpConnect {
    
    /**
     * get请求传参
     * @param param
     * @return
     */
    public String toGETParam(Map<String,Object> param) ;
    
    /**
     * param方式传参
     * @param param
     * @return
     */
    public List<NameValuePair> toPOSTParam(Map<String,Object> param) ;
    
    /**
     * 将返回的流转还为String字符串
     * @param in
     * @return
     */
    public String StreamToStr(InputStream in) ;
    
    

}
