package http;

import org.apache.http.config.ConnectionConfig;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Description:请求对象。用于处理请求公用部分	
 * @author niki
 * @date 2016年12月13日 下午9:25:12
 */
public abstract class Request {
    private Logger logger = LoggerFactory.getLogger(Request.class) ;
    private BasicCookieStore cookieStore = null ; //请求cookie配置
    private ConnectionConfig connConfig = null ;//链接配置
    
    public BasicCookieStore cookie(String name, String value){
        return cookie(name, value, ".mycompany.com", "/", false) ;
    }
    
    public BasicCookieStore cookie(String name, String value, String domain, String path, Boolean secure){
        if(cookieStore == null) cookieStore = new BasicCookieStore() ;
        BasicClientCookie cookie = new BasicClientCookie(name, value) ;
        if(domain == null) domain = "mycompany.com" ;
        if(path == null) path = "/" ;
        if(secure == null) secure = false ;
        cookie.setDomain(domain);
        cookie.setPath(path);
        cookie.setSecure(secure);
        cookieStore.addCookie(cookie);
        return cookieStore ;
    }
    
    public 
    public InputStream connect(){
        return null ;
    }
}
