package http.tool;

import java.io.BufferedReader ;
import java.io.ByteArrayInputStream ;
import java.io.ByteArrayOutputStream ;
import java.io.Closeable ;
import java.io.File ;
import java.io.FileInputStream ;
import java.io.FileOutputStream ;
import java.io.IOException ;
import java.io.InputStream ;
import java.io.InputStreamReader ;
import java.io.PrintWriter ;
import java.io.StringWriter ;
import java.sql.ResultSet ;
import java.sql.SQLException ;
import java.sql.Statement ;
import java.util.ArrayList ;
import java.util.Collection ;
import java.util.Iterator ;
import java.util.List ;
import java.util.StringTokenizer ;

public class Tools {

    private Tools() {
    }

    public static byte[] readResourceBytes(String resourceName) {
        InputStream is = Tools.class.getResourceAsStream(resourceName);

        byte[] e;
        try {
            e = bytes(is);
        } catch (IOException var6) {
            throw new RuntimeException(var6);
        } finally {
            closeQuietly((Closeable)is);
        }

        return e;
    }

    public static String readResource(String resourceName) {
        return readResource(resourceName, "UTF-8");
    }

    public static String readResource(String resourceName, String charset) {
        InputStream is = Tools.class.getResourceAsStream(resourceName);

        String e;
        try {
            e = read(is, charset);
        } catch (IOException var7) {
            throw new RuntimeException(var7);
        } finally {
            closeQuietly((Closeable)is);
        }

        return e;
    }

    public static String readFile(String fileName) {
        return readFile(fileName, "UTF-8");
    }

    public static String readFile(String fileName, String charset) {
        FileInputStream in = null;

        String e;
        try {
            in = new FileInputStream(fileName);
            e = read(in, charset);
        } catch (IOException var7) {
            throw new RuntimeException(var7);
        } finally {
            closeQuietly((Closeable)in);
        }

        return e;
    }

    /** @deprecated */
    @Deprecated
    public static void close(Closeable c) {
        try {
            if(c != null) {
                c.close();
            }

        } catch (IOException var2) {
            throw new RuntimeException(var2);
        }
    }

    public static void closeQuietly(Closeable c) {
        try {
            if(c != null) {
                c.close();
            }
        } catch (IOException var2) {
            ;
        }

    }

    public static void closeQuietly(ResultSet rs) {
        try {
            if(rs != null) {
                rs.close();
            }
        } catch (SQLException var2) {
            ;
        }

    }

    public static void closeQuietly(Statement st) {
        try {
            if(st != null) {
                st.close();
            }
        } catch (SQLException var2) {
            ;
        }

    }

    public static String read(InputStream in) throws IOException {
        return read(in, "UTF-8");
    }

    public static String read(InputStream in, String charset) throws IOException {
        if(in == null) {
            throw new IllegalArgumentException("input stream cannot be null");
        } else {
            InputStreamReader reader = null;

            try {
                reader = new InputStreamReader(in, charset);
                char[] buffer = new char[1024];
                StringBuilder sb = new StringBuilder();

                int len;
                while((len = reader.read(buffer)) != -1) {
                    sb.append(buffer, 0, len);
                }

                String var6 = sb.toString();
                return var6;
            } finally {
                closeQuietly((Closeable)reader);
            }
        }
    }

    public static byte[] bytes(InputStream in) throws IOException {
        if(in == null) {
            throw new IllegalArgumentException("input stream cannot be null");
        } else {
            ByteArrayOutputStream os = null;

            try {
                os = new ByteArrayOutputStream(1024);
                byte[] bytes = new byte[1024];

                int len;
                while((len = in.read(bytes)) != -1) {
                    os.write(bytes, 0, len);
                }

                byte[] var4 = os.toByteArray();
                return var4;
            } finally {
                closeQuietly((Closeable)os);
            }
        }
    }

    public static byte[] read(File file) throws IOException {
        FileInputStream is = new FileInputStream(file);

        byte[] var2;
        try {
            var2 = bytes(is);
        } finally {
            closeQuietly((Closeable)is);
        }

        return var2;
    }

    public static List<String> getResourceLines(String resourceName) throws IOException {
        InputStreamReader isreader = null;
        BufferedReader reader = null;

        try {
            isreader = new InputStreamReader(Tools.class.getResourceAsStream(resourceName));
            reader = new BufferedReader(isreader);
            ArrayList lines = new ArrayList();

            String tmp;
            while((tmp = reader.readLine()) != null) {
                lines.add(tmp);
            }

            ArrayList var5 = lines;
            return var5;
        } finally {
            closeQuietly((Closeable)reader);
            closeQuietly((Closeable)isreader);
        }
    }

    public static boolean blank(Object value) {
        if(value == null) {
            return true;
        } else {
            String str = value.toString();
            if(str != null && !str.isEmpty()) {
                for(int i = 0; i < str.length(); ++i) {
                    if(str.charAt(i) > 32) {
                        return false;
                    }
                }
            }

            return true;
        }
    }

    public static boolean empty(Object[] array) {
        return array == null || array.length == 0;
    }

    public static boolean empty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static String join(String[] array, String delimiter) {
        if(empty((Object[])array)) {
            return "";
        } else {
            StringBuilder sb = new StringBuilder();
            join(sb, (Object[])array, delimiter);
            return sb.toString();
        }
    }

    public static String[] split(String input, String delimiters) {
        if(input == null) {
            throw new NullPointerException("input cannot be null");
        } else {
            ArrayList tokens = new ArrayList();
            StringTokenizer st = new StringTokenizer(input, delimiters);

            while(st.hasMoreTokens()) {
                tokens.add(st.nextToken().trim());
            }

            return (String[])tokens.toArray(new String[tokens.size()]);
        }
    }

    public static String[] split(String input, char delimiter) {
        return split(input, String.valueOf(delimiter));
    }

    public static String join(Collection<?> collection, String delimiter) {
        if(collection.isEmpty()) {
            return "";
        } else {
            StringBuilder sb = new StringBuilder();
            join(sb, collection, delimiter);
            return sb.toString();
        }
    }

    public static void join(StringBuilder sb, Collection<?> collection, String delimiter) {
        if(!collection.isEmpty()) {
            Iterator it = collection.iterator();
            sb.append(it.next());

            while(it.hasNext()) {
                sb.append(delimiter);
                sb.append(it.next());
            }

        }
    }

    public static void join(StringBuilder sb, Object[] array, String delimiter) {
        if(!empty(array)) {
            sb.append(array[0]);

            for(int i = 1; i < array.length; ++i) {
                sb.append(delimiter);
                sb.append(array[i]);
            }

        }
    }

    public static void repeat(StringBuilder sb, String str, int count) {
        for(int i = 0; i < count; ++i) {
            sb.append(str);
        }

    }

    public static void joinAndRepeat(StringBuilder sb, String str, String delimiter, int count) {
        if(count > 0) {
            sb.append(str);

            for(int i = 1; i < count; ++i) {
                sb.append(delimiter);
                sb.append(str);
            }
        }

    }

    public static void saveTo(String path, InputStream in) {
        if(in == null) {
            throw new IllegalArgumentException("input stream cannot be null");
        } else if(path == null) {
            throw new IllegalArgumentException("path cannot be null");
        } else {
            FileOutputStream out = null;

            try {
                out = new FileOutputStream(path);
                byte[] e = new byte[1024];

                int len;
                while((len = in.read(e)) != -1) {
                    out.write(e, 0, len);
                }

                out.flush();
            } catch (IOException var8) {
                throw new RuntimeException(var8);
            } finally {
                closeQuietly((Closeable)out);
            }
        }
    }

    public static String getStackTraceString(Throwable throwable) {
        StringWriter sw = null;
        PrintWriter pw = null;

        String var3;
        try {
            sw = new StringWriter();
            pw = new PrintWriter(sw);
            pw.println(throwable.toString());
            throwable.printStackTrace(pw);
            pw.flush();
            var3 = sw.toString();
        } finally {
            closeQuietly((Closeable)pw);
            closeQuietly((Closeable)sw);
        }

        return var3;
    }

    public static void saveTo(String path, byte[] content) {
        ByteArrayInputStream is = null;

        try {
            is = new ByteArrayInputStream(content);
            saveTo(path, (InputStream)is);
        } finally {
            closeQuietly((Closeable)is);
        }

    }

    public static String toBase64(byte[] input) {
        return Base64.getEncoder().encodeToString(input);
    }

    public static byte[] fromBase64(String input) {
        return Base64.getDecoder().decode(input);
    }
    
    public static boolean isHTTPs(String url){
        return url.split(":")[0].trim().toLowerCase().equals("https") ? true : false ;
    }

}
