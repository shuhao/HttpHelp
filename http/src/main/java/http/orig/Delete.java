package http.orig;

import http.exception.HttpException ;

/**
 * desc:
 * user: Alec Fan
 * date: 2016/9/30
 * time: 17:54
 */
public class Delete extends Request<Delete> {
    public Delete(String uri, int connectTimeout, int readTimeout) {
        super(uri, connectTimeout, readTimeout);
    }

    public Delete doConnect() {
        try {
            this.connection.setDoOutput(true);
            this.connection.setRequestMethod("DELETE");
            this.connection.connect();
            return this;
        } catch (Exception var2) {
            throw new HttpException("Failed URL: " + this.url, var2);
        }
    }

    public static void main(String[] args) {
        Delete delete = Http.delete("http://localhost:8080/kitchensink/http/delete");
        System.out.println(delete.text());
        System.out.println(delete.headers());
        System.out.println(delete.responseCode());
    }
}

