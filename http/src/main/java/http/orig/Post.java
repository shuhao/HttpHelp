package http.orig;

import http.exception.HttpException ;

import java.io.OutputStream;

import org.apache.http.protocol.HTTP ;

/**
 * desc:
 * user: Alec Fan
 * date: 2016/9/30
 * time: 17:59
 */
public class Post extends Request<Post> {
    private final byte[] content;

    public Post(String uri, byte[] content, int connectTimeout, int readTimeout) {
        super(uri, connectTimeout, readTimeout);
        this.content = content;
    }
    
    public Post(String uri, byte[] content, int connectTimeout, int readTimeout,String keypath,String keypwd, String keytype, String teypath,String teypwd, String teytype) {
        super(uri, connectTimeout, readTimeout, keypath, keypwd, keytype, teypath, teypwd, teytype) ;
        this.content = content;
    }

    public Post doConnect() {
        try {
            this.connection.setDoInput(true);
            this.connection.setDoOutput(true);
            this.connection.setUseCaches(false);
            this.connection.setRequestMethod("POST");
            OutputStream e = this.connection.getOutputStream();
            e.write(this.content);
            e.flush();
            return this;
        } catch (Exception var2) {
            throw new HttpException("Failed URL: " + this.url, var2);
        }
    }

    public static void main(String[] args) {
        Post post = Http.post("https://www.baidu.com", "")    ;
        System.out.println(post.text()) ;
    }
}
