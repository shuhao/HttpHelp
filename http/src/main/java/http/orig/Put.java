package http.orig;

import http.exception.HttpException ;

import java.io.OutputStream;

/**
 * desc:
 * user: Alec Fan
 * date: 2016/9/30
 * time: 17:59
 */
public class Put extends Request<Put> {
    private final byte[] content;

    public Put(String uri, byte[] content, int connectTimeout, int readTimeout) {
        super(uri, connectTimeout, readTimeout);
        this.content = content;
    }

    public Put doConnect() {
        try {
            this.connection.setDoOutput(true);
            this.connection.setDoInput(true);
            this.connection.setRequestMethod("PUT");
            OutputStream e = this.connection.getOutputStream();
            e.write(this.content);
            e.flush();
            return this;
        } catch (Exception var2) {
            throw new HttpException("Failed URL: " + this.url, var2);
        }
    }

    public static void main(String[] args) {
        Put put = Http.put("http://localhost:8080/kitchensink/http/put", "bugagaga");
        System.out.println(put.text());
        System.out.println(put.headers());
        System.out.println(put.responseCode());
    }
}
