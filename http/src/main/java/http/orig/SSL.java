package http.orig;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/**
 * Description:https请求处理	
 * @author niki
 * @date 2016年12月14日 下午10:03:32
 */
public class SSL {
    static class MyX509TrustManager implements X509TrustManager{
        /*
         * 该方法检查客户端的证书，若不信任该证书则抛出异常。由于我们不需要对客户端进行认证，因此我们只需要执行默认的信任管理器的这个方法。JSSE中，默认的信任管理器类为TrustManager。
         */
        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
        }
        /*
         * 该方法检查服务器的证书，若不信任该证书同样抛出异常。通过自己实现该方法，可以使之信任我们指定的任何证书。在实现该方法时，也可以简单的不做任何处理，即一个空的函数体，由于不会抛出异常，它就会信任任何证书。
         */
        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
        }

        public X509Certificate[] getAcceptedIssuers() {
            
            return null;
        }
        
    }
    private static String defaultProtocol = "SSL" ;
    /**
     * 单向验证，跳过ssl验证
     * @author niki
     * @date 2016年12月14日 下午10:04:36
     * @return
     */
    public static HttpsURLConnection addSSL(HttpsURLConnection connection){
        if(connection != null){
            TrustManager[] trusts = {new MyX509TrustManager()} ;
            try {
                SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE") ;
                sslContext.init(null, trusts, null);
                SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory() ;
                connection.setSSLSocketFactory(sslSocketFactory);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchProviderException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }
            
        }
        return connection ;
    }
    
    public static HttpsURLConnection addSSL(HttpsURLConnection connection, String keyPath, String keyPwd, String keyType, String trustPath, String trustPwd, String trustType, String sslProtocol){
        if(connection != null){
            if(sslProtocol == null || sslProtocol.trim().equals("")) sslProtocol = defaultProtocol ;
            System.setProperty("javax.net.ssl.keyStore",  keyPath) ;
            System.setProperty("javax.net.ssl.keyStorePassword", keyPwd) ;
            System.setProperty("javax.net.ssl.keyStoreType", keyType) ;
            System.setProperty("javax.net.ssl.trustStore",  trustPath) ;
            System.setProperty("javax.net.ssl.trustStorePassword", trustPwd) ;
            System.setProperty("javax.net.ssl.trustStoreType", trustType) ;
            
            try {
                KeyStore keystore = KeyStore.getInstance(keyType) ;
                keystore.load(new FileInputStream(keyPath), keyPwd.toCharArray());
                KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm()) ;
                kmf.init(keystore, keyPwd.toCharArray());
                KeyManager[] km = kmf.getKeyManagers() ;
                
                KeyStore trusttore = KeyStore.getInstance(trustType) ;
                trusttore.load(new FileInputStream(trustPath), trustPwd.toCharArray());
                TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm()) ;
                tmf.init(trusttore);
                TrustManager[] tms = tmf.getTrustManagers() ;
                SSLContext sslcontext = SSLContext.getInstance(sslProtocol) ;
                sslcontext.init(km, tms, new java.security.SecureRandom());
                connection.setSSLSocketFactory(sslcontext.getSocketFactory());
            } catch (KeyStoreException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (CertificateException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (UnrecoverableKeyException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }
        }
        return connection ;
    }
}
