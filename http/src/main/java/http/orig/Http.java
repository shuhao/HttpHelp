package http.orig;

import http.exception.HttpException ;

import java.net.URLEncoder;
import java.util.Map;
import java.util.Set;

/**
 * desc:
 * user: Alec Fan
 * date: 2016/9/30
 * time: 17:58
 */
public class Http {
    public static int CONNECTION_TIMEOUT = 5000;
    public static int READ_TIMEOUT = 5000;

    public Http() {
    }

    public static Post post(String uri, String content) {
        return post(uri, content.getBytes(), CONNECTION_TIMEOUT, READ_TIMEOUT);
    }
    
    public static Post post(String uri, String content,String keypath, String keypwd,String keyType, String trustpath, String trustpwd,String trustType ) {
        return post(uri, content.getBytes(), CONNECTION_TIMEOUT, READ_TIMEOUT,keypath, keypwd, keyType, trustpath, trustpwd, trustType);
    }

    public static Post post(String uri, byte[] content) {
        return post(uri, content, CONNECTION_TIMEOUT, READ_TIMEOUT);
    }

    public static Post post(String url, byte[] content, int connectTimeout, int readTimeout,String keypath, String keypwd,String keyType, String trustpath, String trustpwd,String trustType) {
        try {
            return new Post(url, content, connectTimeout, readTimeout,keypath, keypwd, keyType, trustpath, trustpwd, trustType);
        } catch (Exception var5) {
            throw new HttpException("Failed URL: " + url, var5);
        }
    }
    
    public static Post post(String url, byte[] content, int connectTimeout, int readTimeout) {
        try {
            return new Post(url, content, connectTimeout, readTimeout);
        } catch (Exception var5) {
            throw new HttpException("Failed URL: " + url, var5);
        }
    }

    public static Get get(String url) {
        return get(url, CONNECTION_TIMEOUT, READ_TIMEOUT);
    }

    public static Get get(String url, int connectTimeout, int readTimeout) {
        try {
            return new Get(url, connectTimeout, readTimeout);
        } catch (Exception var4) {
            throw new HttpException("Failed URL: " + url, var4);
        }
    }

    public static Put put(String uri, String content) {
        return put(uri, content.getBytes());
    }

    public static Put put(String uri, byte[] content) {
        return put(uri, content, CONNECTION_TIMEOUT, READ_TIMEOUT);
    }

    public static Put put(String url, byte[] content, int connectTimeout, int readTimeout) {
        try {
            return new Put(url, content, connectTimeout, readTimeout);
        } catch (Exception var5) {
            throw new HttpException("Failed URL: " + url, var5);
        }
    }

    public static Delete delete(String uri) {
        return delete(uri, CONNECTION_TIMEOUT, READ_TIMEOUT);
    }

    public static Delete delete(String url, int connectTimeout, int readTimeout) {
        try {
            return new Delete(url, connectTimeout, readTimeout);
        } catch (Exception var4) {
            throw new HttpException("Failed URL: " + url, var4);
        }
    }

    public static String map2Content(Map params) {
        StringBuilder stringBuilder = new StringBuilder();

        try {
            Set e = params.keySet();
            Object[] keys = e.toArray();

            for(int i = 0; i < keys.length; ++i) {
                stringBuilder.append(URLEncoder.encode(keys[i].toString(), "UTF-8")).append("=").append(URLEncoder.encode(params.get(keys[i]).toString(), "UTF-8"));
                if(i < keys.length - 1) {
                    stringBuilder.append("&");
                }
            }
        } catch (Exception var5) {
            throw new HttpException("failed to generate content from map", var5);
        }

        return stringBuilder.toString();
    }
}
