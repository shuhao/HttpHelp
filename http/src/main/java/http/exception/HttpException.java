package http.exception;

/**
 * desc:
 * user: Alec Fan
 * date: 2016/9/30
 * time: 17:55
 */
public class HttpException extends RuntimeException {
    public HttpException(String message, Throwable cause) {
        super(message, cause);
    }
}
