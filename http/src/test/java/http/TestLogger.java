package http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestLogger {
    public static void main(String[] args) {
        Logger logger = LoggerFactory.getLogger(TestLogger.class) ;
        logger.debug("输出debug级别的日志.....");  
        logger.info("输出info级别的日志.....");  
        logger.error("输出error级别的日志.....");  
    }
}
