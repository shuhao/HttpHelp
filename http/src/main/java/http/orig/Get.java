package http.orig;

import http.exception.HttpException ;

/**
 * desc:
 * user: Alec Fan
 * date: 2016/9/30
 * time: 17:59
 */
public class Get extends Request<Get> {
    public Get(String uri, int connectTimeout, int readTimeout) {
        super(uri, connectTimeout, readTimeout);
    }

    public Get doConnect() {
        try {
            this.connection.setDoInput(true);
            this.connection.setDoOutput(true);
            this.connection.setUseCaches(false);
            this.connection.setRequestMethod("GET");
            return this;
        } catch (Exception var2) {
            throw new HttpException("Failed URL: " + this.url, var2);
        }
    }

    public static void main(String[] args) {
        Get get = (Get)Http.get("http://localhost:8080/manager/html").basic("tomcat", "tomcat");
        System.out.println(get.text());
        System.out.println(get.headers());
        System.out.println(get.responseCode());
    }
}

