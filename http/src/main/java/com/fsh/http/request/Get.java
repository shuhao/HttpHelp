package com.fsh.http.request;

import java.net.HttpURLConnection ;
import java.net.ProtocolException ;

import com.fsh.http.exception.HttpException ;
import com.fsh.http.util.Constant ;
/**
 * get请求对象 
 * Description:	
 * @author 80002165
 * @date 2017年1月17日 下午5:26:44
 */
public class Get extends Request<Get>{
    
    public Get(String url, int connectTimeout, int readTimeout) {
        super(url, connectTimeout, readTimeout) ;
    }

    @Override
    protected Get doConnect() {
        defDoInputOrOutput();
        try {
            this.connection.setRequestMethod(Constant.HTTP_METHOD_GET);
        } catch (ProtocolException e) {
            throw new HttpException("Failed url：" + this.url, e) ;
        }
        return this ;
    }
    
}
