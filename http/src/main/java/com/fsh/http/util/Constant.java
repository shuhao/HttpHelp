package com.fsh.http.util;
/**
 * http常量类
 * Description:	
 * @author 80002165
 * @date 2017年1月17日 下午6:15:08
 */
public class Constant {
    public final static String HTTP_METHOD_POST = "POST" ;
    public final static String HTTP_METHOD_GET = "GET" ;
    public final static String DEF_ENCODING = "UTF-8" ;
}
