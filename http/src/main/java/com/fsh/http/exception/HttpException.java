package com.fsh.http.exception ;
/**
 * 异常类
 * Description:	
 * @author 80002165
 * @date 2017年1月17日 下午6:14:29
 */
public class HttpException extends RuntimeException {
    public HttpException(String message, Throwable cause) {
        super(message, cause) ;
    }
}
