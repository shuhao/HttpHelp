package com.fsh.http.util ;

import java.io.Closeable ;
import java.io.IOException ;
import java.io.InputStream ;
import java.io.InputStreamReader ;


import com.fsh.http.exception.HttpException ;

/**
 * 关于请求的一些工具方法 
 * Description: 该类不可修改
 * 
 * @author 80002165
 * @date 2017年1月17日 下午2:38:05
 */
public final class Util {
    public static boolean isEmpty(String string) {
        return (string == null) || (string.trim().isEmpty()) ;
    }
    
    public static String read(InputStream input, String encoding) {
        if (input == null)
            throw new IllegalArgumentException("your input stream is null!") ;
        if (isEmpty(encoding))
            encoding = "UTF-8" ;
        InputStreamReader reader = null ;
        try {
            int length = input.available() ;
            reader = new InputStreamReader(input, encoding) ;
            char[] cache = new char[length] ;
            int len = reader.read(cache) ;
            return new String(cache, 0, len) ;
        } catch (IOException e) {
            throw new HttpException("failed read String!", e) ;
        } finally {
            quietlyCloseStream(reader);
        }
    }
    
    public static String read(InputStream input) {
        return read(input, null) ;
    }
    
    public static void quietlyCloseStream(Closeable closeObj){
        if(closeObj != null){
            try {
                closeObj.close();
            } catch (IOException e) {
               ;
            } 
        }
    }
    
    public static void main(String[] args) {
    }
}
